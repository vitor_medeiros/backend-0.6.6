<p>Recebemos uma solicitação para recuperar sua senha. Para definir uma nova senha, você deve acessar o link a seguir: <br><br>

<a href="{{$url}}/session/redefinir/{{$id}}/{{$token}}">Criar uma nova senha</a> <br><br>

Em seguida, siga as orientações na tela do link acessado. <br><br>

Obs.: caso você não tenha solicitado a sua senha de acesso, favor ignorar este e-mail. <br><br>


Atenciosamente, <br>
Eletro+ Tecnologia
</p>	