<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], static function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group(['prefix' => 'teste'], static function () {
    Route::apiResource('dispositivo', 'DispositivoController');
    Route::apiResource('dispositivo-alocado', 'DispositivoAlocadoController', ['except' => ['store']]);
    Route::apiResource('estacao', 'EstacaoController');
    Route::apiResource('estado-dispositivo', 'EstadoDispositivoController');
    Route::apiResource('evento', 'EventoController');
    Route::apiResource('municipio', 'MunicipioController');
    Route::apiResource('processo', 'ProcessoController');
    Route::apiResource('produto', 'ProdutoController');
    Route::apiResource('tipo-dispositivo', 'TipoDispositivoController');
    Route::apiResource('tipo-evento', 'TipoEventoController');
    Route::apiResource('usuario-estacao', 'UsuarioEstacaoController', ['except' => ['store']]);
    Route::apiResource('versao', 'VersaoController', ['except' => ['destroy']]);
    Route::apiResource('versao-disponivel', 'VersaoDisponivelController');
    Route::get('sincroniza/{id}', 'SincronizaController@show');
    Route::delete('versao/{versao}', 'VersaoControllerDestroy@destroy')->name('versao.destroy');
    Route::post('usuario-estacao', 'UsuarioEstacaoControllerStore@store')->name('usuario-estacao.store');
    Route::post('dispositivo-alocado', 'DispositivoAlocadoControllerStore@store')->name('dispositivo-alocado.store');
    Route::delete('usuario/{usuario}', 'UsuarioControllerDestroy@destroy')->name('usuario.destroy');
    Route::post('usuario/recuperar-senha', 'UsuarioControllerRecuperarSenha@recuperarSenha')->name('usuario.recuperar-senha');
    Route::post('usuario/alterar-senha', 'UsuarioControllerRecuperarSenha@alterarSenha')->name('usuario.alterar-senha');
});
