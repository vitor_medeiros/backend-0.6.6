<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVersaoDisponivelTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'versao_disponivel';

    /**
     * Run the migrations.
     * @table versao_disponivel
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
//            $table->engine = 'InnoDB';
            $table->increments('id_versao_disponivel');
            $table->unsignedInteger('cd_estacao');
            $table->unsignedInteger('cd_versao');
            $table->dateTime('data_liberacao')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index(["cd_versao"], 'fk_versao_disponivel_versao1_idx');

            $table->index(["cd_estacao"], 'fk_versao_disponivel_estacao1_idx');


            $table->foreign('cd_estacao', 'fk_versao_disponivel_estacao1_idx')
                ->references('id_estacao')->on('estacao')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cd_versao', 'fk_versao_disponivel_versao1_idx')
                ->references('id_versao')->on('versao')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
