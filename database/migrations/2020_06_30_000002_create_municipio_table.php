<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMunicipioTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'municipio';

    /**
     * Run the migrations.
     * @table municipio
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
//            $table->engine = 'InnoDB';
            $table->increments('id_municipio');
            $table->string('municipio', 100)->nullable();
            $table->string('uf', 2)->nullable();
            $table->string('ibge', 7)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
        DB::unprepared(file_get_contents(database_path('migrations/municipios.sql')));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
