<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ManyChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dispositivo_alocado', static function (Blueprint $table) {
            $table->foreign('cd_sinal')
                ->references('id_enum')
                ->on('enum');
            $table->dropForeign('dispositivo_alocado_cd_estado_foreign');
            $table->foreign('cd_estado')
                ->references('id_enum')
                ->on('enum');
        });
        Schema::table('evento', static function (Blueprint $table) {
            $table->dropForeign('fk_evento_tipo_evento1_idx');
            $table->foreign('cd_tipo_evento')
                ->references('id_enum')
                ->on('enum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
