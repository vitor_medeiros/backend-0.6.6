<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEstacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('estacao', static function (Blueprint $table) {
            $table->renameColumn('descricao', 'nome');
            $table->renameColumn('cd_estado', 'cd_situacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estacao', static function (Blueprint $table) {
            $table->renameColumn('nome', 'descricao');
            $table->renameColumn('cd_situacao', 'cd_estado');
        });
    }
}
