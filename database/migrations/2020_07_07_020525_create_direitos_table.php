<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDireitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo', static function (Blueprint $table) {
            $table->id('id_grupo');
            $table->string('nome')->unique();
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('usuario_grupo', static function (Blueprint $table) {
            $table->id('id_usuario_grupo');
            $table->bigInteger('cd_grupo');
            $table->bigInteger('cd_usuario');

            $table->foreign('cd_grupo')
                ->references('id_grupo')
                ->on('grupo');
            $table->foreign('cd_usuario')
                ->references('id_usuario')
                ->on('usuario');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo');
        Schema::dropIfExists('usuario_grupo');
    }
}
