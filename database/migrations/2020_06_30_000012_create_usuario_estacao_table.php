<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioEstacaoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'usuario_estacao';

    /**
     * Run the migrations.
     * @table usuario_estacao
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
////            $table->engine = 'InnoDB';
            $table->increments('id_usuario_estacao');
            $table->unsignedInteger('cd_estacao');
            $table->unsignedBigInteger('cd_usuario');
            $table->boolean('vende')->default(true);

            $table->timestamps();
            $table->softDeletes();

            $table->index(["cd_estacao"], 'fk_usuario_estacao_estacao1_idx');

            $table->index(["cd_usuario"], 'fk_usuario_estacao_usuario1_idx');


            $table->foreign('cd_estacao', 'fk_usuario_estacao_estacao1_idx')
                ->references('id_estacao')->on('estacao')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cd_usuario', 'fk_usuario_estacao_usuario1_idx')
                ->references('id_usuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
