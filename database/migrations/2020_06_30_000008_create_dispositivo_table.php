<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDispositivoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'dispositivo';

    /**
     * Run the migrations.
     * @table dispositivo
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
//            $table->engine = 'InnoDB';
            $table->increments('id_dispositivo');
            $table->unsignedInteger('cd_tipo_dispositivo');
            $table->unsignedInteger('cd_versao');
            $table->string('numero_serie', 20)->nullable();
            $table->string('modelo', 50)->nullable();
            $table->string('lote', 20)->nullable();
            $table->date('data_producao')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index(["cd_tipo_dispositivo"], 'fk_dispositivo_tipo_dispositivo1_idx');


            $table->foreign('cd_tipo_dispositivo', 'fk_dispositivo_tipo_dispositivo1_idx')
                ->references('id_tipo_dispositivo')->on('tipo_dispositivo')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cd_versao')
                ->references('id_versao')->on('versao')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
