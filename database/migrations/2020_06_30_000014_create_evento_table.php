<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'evento';

    /**
     * Run the migrations.
     * @table evento
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
//            $table->engine = 'InnoDB';
            $table->increments('id_evento');
            $table->unsignedInteger('cd_dispositivo_alocado');
            $table->unsignedInteger('cd_tipo_evento');
            $table->dateTime('data_hora')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index(["cd_dispositivo_alocado"], 'fk_evento_dispositivo_alocado1_idx');

            $table->index(["cd_tipo_evento"], 'fk_evento_tipo_evento1_idx');


            $table->foreign('cd_dispositivo_alocado', 'fk_evento_dispositivo_alocado1_idx')
                ->references('id_dispositivo_alocado')->on('dispositivo_alocado')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cd_tipo_evento', 'fk_evento_tipo_evento1_idx')
                ->references('id_tipo_evento')->on('tipo_evento')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
