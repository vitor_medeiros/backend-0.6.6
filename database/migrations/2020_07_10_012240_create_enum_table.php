<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateEnumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enum', static function (Blueprint $table) {
            $table->id('id_enum');
            $table->integer('codigo');
            $table->string('descricao');
            $table->string('observacao')->nullable();
            $table->string('entidade');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('estacao', static function (Blueprint $table) {
            $table->dropColumn(['estado', 'contrato']);
            $table->bigInteger('cd_enum_estado');
            $table->bigInteger('cd_enum_contrato');

            $table->foreign('cd_enum_estado')
                ->references('id_enum')
                ->on('enum');

            $table->foreign('cd_enum_contrato')
                ->references('id_enum')
                ->on('enum');
        });
        #DB::unprepared("Insert into enum (nome) values ('Preparação');");
       # DB::unprepared("Insert into enum (nome) values ('Período Teste');");
        #DB::unprepared("Insert into enum (nome) values ('Ativo');");
        #DB::unprepared("Insert into enum (nome) values ('Inativo');");
        #DB::unprepared("Insert into enum (nome) values ('Por banho');");
        #DB::unprepared("Insert into enum (nome) values ('Por máquina');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enum');
    }
}
