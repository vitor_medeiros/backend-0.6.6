<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropTablesForEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dispositivo_alocado', static function(Blueprint $table){
            $table->dropForeign('fk_dispositivo_alocado_estado_dispositivo1_idx');
        });
        Schema::dropIfExists('tipo_evento');
        Schema::dropIfExists('estado_dispositivo');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
