<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'processo';

    /**
     * Run the migrations.
     * @table processo
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
//            $table->engine = 'InnoDB';
            $table->increments('id_processo');
            $table->unsignedInteger('cd_produto');
            $table->unsignedInteger('cd_usuario');
            $table->unsignedInteger('cd_dispositivo_alocado');
            $table->dateTime('data_hora_liberacao')->nullable();
            $table->dateTime('data_hora_inicio')->nullable();
            $table->dateTime('data_hora_fim')->nullable();
            $table->integer('segundos_pausas')->nullable();
            $table->integer('pausas')->nullable();
            $table->boolean('violado')->nullable();
            $table->integer('litro_agua')->nullable();
            $table->decimal('vazao_agua_media', 9, 3)->nullable();
            $table->decimal('energia_kw_h', 9, 3)->nullable();
            $table->string('observacao')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index(["cd_produto"], 'fk_processo_produto1_idx');

            $table->index(["cd_dispositivo_alocado"], 'fk_processo_dispositivo_alocado1_idx');


            $table->foreign('cd_produto', 'fk_processo_produto1_idx')
                ->references('id_produto')->on('produto')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cd_dispositivo_alocado', 'fk_processo_dispositivo_alocado1_idx')
                ->references('id_dispositivo_alocado')->on('dispositivo_alocado')
                ->onDelete('no action')
                ->onUpdate('no action');

             $table->foreign('cd_usuario', 'fk_usuario_estacao_usuario1_idx')
                ->references('id_usuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
