<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeCdEstacaoNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('versao_disponivel', static function (Blueprint $table) {
            $table->unsignedInteger('cd_estacao')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('versao_disponivel', static function (Blueprint $table) {
            $table->unsignedInteger('cd_estacao')->change();
        });
    }
}
