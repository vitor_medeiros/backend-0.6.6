<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstacaoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'estacao';

    /**
     * Run the migrations.
     * @table estacao
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
//            $table->engine = 'InnoDB';
            $table->increments('id_estacao');
            $table->unsignedInteger('cd_municipio');
            $table->string('codigo', 10)->nullable();
            $table->string('descricao', 100)->nullable();
            $table->integer('maquinas_liberadas')->nullable();
            $table->string('cnpj_empresa', 20)->nullable();
            $table->string('nome_empresa', 100)->nullable();
            $table->integer('dia_fechamento')->nullable();
            $table->integer('dia_vencimento')->nullable();
            $table->date('data_periodo_atual')->nullable();
            $table->date('data_ultimo_periodo_pago')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->string('observacoes')->nullable();
            $table->integer('estado')->nullable();
            $table->date('data_ultimo_contrato')->nullable();
            $table->integer('contrato')->nullable();
            $table->double('valor_por_unidade', 9, 2)->nullable();
            $table->string('nome_rede', 100)->nullable();

            $table->timestamps();
            $table->softDeletes();


            $table->index(["cd_municipio"], 'fk_estacao_municipio1_idx');


            $table->foreign('cd_municipio', 'fk_estacao_municipio1_idx')
                ->references('id_municipio')->on('municipio')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
