<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDispositivoAlocadoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'dispositivo_alocado';

    /**
     * Run the migrations.
     * @table dispositivo_alocado
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
//            $table->engine = 'InnoDB';
            $table->increments('id_dispositivo_alocado');
            $table->unsignedInteger('cd_dispositivo');
            $table->unsignedInteger('cd_estacao');
            $table->unsignedInteger('cd_estado');
            $table->dateTime('data_hora_inicio_alocacao')->nullable();
            $table->dateTime('data_hora_fim_alocacao')->nullable();
            $table->dateTime('data_hora_ultima_comunicacao')->nullable();
            $table->string('numero_chuveiro', 20)->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index(["cd_estacao"], 'fk_dispositivo_alocado_estacao1_idx');

            $table->index(["cd_dispositivo"], 'fk_dispositivo_alocado_dispositivo1_idx');

            $table->index(["cd_estado"], 'fk_dispositivo_alocado_estado_dispositivo1_idx');


            $table->foreign('cd_dispositivo', 'fk_dispositivo_alocado_dispositivo1_idx')
                ->references('id_dispositivo')->on('dispositivo')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cd_estacao', 'fk_dispositivo_alocado_estacao1_idx')
                ->references('id_estacao')->on('estacao')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cd_estado', 'fk_dispositivo_alocado_estado_dispositivo1_idx')
                ->references('id_estado_dispositivo')->on('estado_dispositivo')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
