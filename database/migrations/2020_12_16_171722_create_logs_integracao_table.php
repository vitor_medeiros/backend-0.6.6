<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsIntegracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs_integracao', function (Blueprint $table) {
            $table->id('id_logs_integracao');
            $table->text('erro_integracao');
            $table->text('mensagem');
            $table->timestamp('data_hora_mensagem');
            $table->timestamp('data_hora_integracao');
            $table->string('tipo_mensagem');
            $table->string('numero_serie');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs_interacao');
    }
}
