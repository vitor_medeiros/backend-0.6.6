<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVersaoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'versao';

    /**
     * Run the migrations.
     * @table versao
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
//            $table->engine = 'InnoDB';
            $table->increments('id_versao');
            $table->unsignedInteger('cd_tipo_dispositivo');
            $table->longText('notas')->nullable();
            $table->string('caminho_arquivo')->nullable();
            $table->dateTime('data_liberacao')->nullable();
            $table->boolean('validada_producao')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index(["cd_tipo_dispositivo"], 'fk_versao_tipo_dispositivo_idx');


            $table->foreign('cd_tipo_dispositivo', 'fk_versao_tipo_dispositivo_idx')
                ->references('id_tipo_dispositivo')->on('tipo_dispositivo')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
