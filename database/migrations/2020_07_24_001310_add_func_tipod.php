<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFuncTipod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipo_dispositivo', static function (Blueprint $table) {
            $table->string('funcao')->nullable();
        });
        Schema::table('estacao', static function (Blueprint $table) {
            $table->renameColumn('cd_enum_estado', 'cd_estado');
            $table->renameColumn('cd_enum_contrato', 'cd_tipo_contrato');
        });
        Schema::table('dispositivo_alocado', static function (Blueprint $table) {
            $table->bigInteger('cd_sinal');
            $table->string('nome_dispositivo', 20);

            $table->foreign('cd_estado')->references('id_enum')->on('enum');
        });

        Schema::table('enum', static function (Blueprint $table) {
            #$table->string('entidade', 100)->default('NONE');// desconheço isso aq, n sou espirita
            #$table->renameColumn('nome', 'descricao');
            #$table->integer('codigo')->nullable();
            #$table->string('observacao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tipo_dispositivo', static function (Blueprint $table) {
            $table->dropColumn('funcao');
        });
        Schema::table('estacao', static function (Blueprint $table) {
            $table->renameColumn('cd_estado', 'cd_enum_estado');
            $table->renameColumn('cd_tipo_contrato', 'cd_enum_contrato');
        });

        Schema::table('dispositivo_alocado', static function (Blueprint $table) {
            $table->dropColumn('cd_sinal');
            $table->dropColumn('nome_dispositivo');
        });
        Schema::table('enum', static function (Blueprint $table) {
            $table->dropColumn('entidade');// baixando a entidade, basicamente um ritual espirita
            $table->renameColumn('descricao', 'nome');
            $table->dropColumn('codigo');
            $table->dropColumn('observacao');
        });
    }
}
