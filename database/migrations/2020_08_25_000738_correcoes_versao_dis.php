<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CorrecoesVersaoDis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('versao_disponivel', static function (Blueprint $table) {
            $table->unique(['cd_estacao', 'cd_versao', 'deleted_at'], 'versao_disponivel_unq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('versao_disponivel', static function (Blueprint $table) {
            $table->dropUnique('versao_disponivel_unq');
        });
    }
}
