<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'produto';

    /**
     * Run the migrations.
     * @table produto
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
//            $table->engine = 'InnoDB';
            $table->increments('id_produto');
            $table->unsignedInteger('cd_estacao');
            $table->unsignedInteger('codigo')->nullable();
            $table->string('descricao', 40)->nullable();
            $table->string('desc_reduzida', 20)->nullable();
            $table->integer('segundos')->nullable();
            $table->integer('pausas')->nullable();
            $table->integer('segundos_pausa')->nullable();
            $table->decimal('valor', 9, 2)->nullable();
            $table->boolean('e_cortesia')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index(["cd_estacao"], 'fk_produto_estacao1_idx');


            $table->foreign('cd_estacao', 'fk_produto_estacao1_idx')
                ->references('id_estacao')->on('estacao')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
