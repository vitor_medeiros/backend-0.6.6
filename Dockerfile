FROM php:7.4.6-cli

RUN apt-get update

# 1. development packages
RUN apt-get install -y \
    git \
    supervisor \
    zip \
    curl \
    sudo \
    unzip \
    libicu-dev \
    libpq-dev \
    libbz2-dev \
    libpng-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libreadline-dev \
    libfreetype6-dev \
    g++

RUN echo_supervisord_conf > /etc/supervisor/conf.d/supervisord.conf
CMD ["/usr/bin/supervisord"]

RUN apt-get install -y nodejs npm

RUN pecl install swoole;
RUN apt-get update -y && apt-get install -y sendmail libpng-dev

RUN apt-get update && \
    apt-get install -y \
        zlib1g-dev

RUN pecl install -o -f redis \
&&  rm -rf /tmp/pear \
&&  docker-php-ext-enable redis

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

RUN docker-php-ext-install \
    bz2 \
    intl \
    iconv \
    bcmath \
    opcache \
    calendar \
    gd \
    pdo_pgsql

RUN docker-php-ext-configure opcache --enable-opcache \
    && docker-php-ext-install opcache

RUN docker-php-ext-install sockets
RUN docker-php-ext-install pcntl

RUN touch /usr/local/etc/php/conf.d/swoole.ini && \
    echo 'extension=swoole.so' > /usr/local/etc/php/conf.d/swoole.ini

RUN touch /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini && \
    echo 'opcache.validate_timestamps=0' > /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

RUN touch /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini && \
        echo 'opcache.enable=1' > /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

RUN touch /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini && \
                echo 'opcache.enable_cli=1' > /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

RUN useradd -G www-data,root -u 123 -d /home/devuser devuser

RUN mkdir -p /home/devuser/.composer && \
    chown -R devuser:devuser /home/devuser

WORKDIR /var/www/html/
COPY . /var/www/html/

WORKDIR /var/www/html/storage/

RUN mkdir -p framework/sessions/
RUN mkdir -p framework/views/
RUN mkdir -p framework/cache/

RUN chmod -R 777 framework
RUN chown -R root framework

WORKDIR /var/www/html/

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer install

RUN npm install
RUN npm run prod

CMD ["php", "artisan","optimize"]

EXPOSE 80
