<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Usuario;

class usuarioRecuperarSenha extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Usuario $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $numeros = "0123456789";
        $letras = "abcdefghijklmnopqrstuvwxyz";
        $token = str_shuffle($numeros) . str_shuffle($letras);
        $token = substr(str_shuffle($token),0,100);
        $usuario = Usuario::find($this->user->id_usuario);
        $id = $usuario->id_usuario;
        $usuario->remember_token = $token;
        $usuario->save();
        $this->subject('Recuperar senha');
        $this->to($this->user->email, $this->user->nome);
        $url = \Config::get('app.url');
        return $this->view('mail.teste', ['token' => $token, 'url' => $url, 'id' => $id]);
    }
}
