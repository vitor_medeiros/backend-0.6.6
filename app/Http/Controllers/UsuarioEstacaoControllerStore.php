<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\UsuarioEstacao;
use App\Models\Estacao;

class UsuarioEstacaoControllerStore extends Controller
{
    public function store(Request $request)
    {
    	if ($request->filled('cd_estacao') and $request->filled('cd_usuario')) {
	    	$cd_usuario = $request->cd_usuario;
	    	$cd_estacao = $request->cd_estacao;
		}else{
			return ['message' => 'The given data was invalid.'];
		}
        try{
            $user = Usuario::find($cd_usuario);
            $estacao = Estacao::find($cd_estacao);
            if ($user) {
            	if (!$estacao) return ['message' => 'Estação inexistente'];
            	$user_estacao = new UsuarioEstacao;
            	$user_estacao->cd_usuario = $cd_usuario;
            	$user_estacao->cd_estacao = $cd_estacao;
            	if ($user->tipo_perfil == 'V') {
            		$user_estacao->vende = true;
            		$user_estacao->save();
            		return ['cd_estacao' => $cd_estacao, 'cd_usuario' => $cd_usuario, 'vende' => true, 'created_at' => $user_estacao->created_at, 'id_usuario_estacao' => $user_estacao->id_usuario_estacao];
            	}else{
            		$user_estacao->vende = false;
            		$user_estacao->save();
            		return ['cd_estacao' => $cd_estacao, 'cd_usuario' => $cd_usuario, 'vende' => false, 'created_at' => $user_estacao->created_at, 'id_usuario_estacao' => $user_estacao->id_usuario_estacao];
            	}
            }else{
            	return ['message' => 'Usuário inexistente'];
            }
        }catch(\ModelNotFoundException $ex){
            return ['message' => $ex->getMessage()];
        }
        
    }
}
