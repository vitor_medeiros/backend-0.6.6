<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\VersaoDisponivelService;

class VersaoDisponivelController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(VersaoDisponivelService::class);
    }
}
