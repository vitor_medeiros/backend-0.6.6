<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\UsuarioEstacaoService;

class UsuarioEstacaoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(UsuarioEstacaoService::class);
    }
}
