<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\EstadoDispositivoService;

class EstadoDispositivoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(EstadoDispositivoService::class);
    }
}
