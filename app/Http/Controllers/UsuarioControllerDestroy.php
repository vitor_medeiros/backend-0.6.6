<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\UsuarioEstacao;
use App\Mail\usuarioRecuperarSenha;


class UsuarioControllerDestroy extends Controller
{
    public function destroy($id)
    {
        try{
            $user = Usuario::find($id);
            if ($user) {
                $usuarios_estacao = UsuarioEstacao::where('cd_usuario', $id)->get();
                if ($usuarios_estacao) {
                    foreach ($usuarios_estacao as $usuario_estacao) {
                        $usuario_estacao->delete();
                    }
                    $user->delete();
                    return ['success' => true];
                }else{
                    $user->delete();
                    return ['success' => true];
                }
              
            }else{
                return ['message' => 'Wrong ID!!'];
            }
        }catch(\ModelNotFoundException $ex){
            return ['message' => $ex->getMessage()];
        }
        
    }

    
}
