<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\ProcessoService;

class ProcessoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(ProcessoService::class);
    }
}
