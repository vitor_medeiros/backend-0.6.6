<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\VersaoService;

class VersaoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(VersaoService::class);
    }
}
