<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\Models\Dispositivo;
use App\Models\Versao;
use App\Models\VersaoDisponivel;


class SincronizaController extends Controller
{
   
    public function show($id)
    {
        $consulta = \DB::select("select numero_serie from dispositivo join dispositivo_alocado on id_dispositivo = cd_dispositivo where cd_estacao = $id and cd_tipo_dispositivo = 1 and dispositivo_alocado.deleted_at IS NULL");
        
        if ($consulta) {
            $numero_serie = $consulta[0]->numero_serie;
            $message = '{"cd_estacao": $id, "estacao": true, "usuarios": true, "produtos": true, "dispositivos": true}'; 
            $queue = 'hub_' . $numero_serie;
         
            // echo 'Log error '.$message . PHP_EOL;

            $host = env('AMQP_HOST', 'amqp.sis.eletromais.tec.br');
            $port =  env('AMQP_PORT', 5672);
            $username = env('AMQP_USERNAME', 'rabbitmq');
            $password = env('AMQP_PASSWORD', 'rabbitmq');
               
            $connection = new AMQPStreamConnection($host, $port , $username, $password);
            $channel = $connection->channel();

            // Message durability
            $channel->queue_declare($queue, false, true, false, false);

            $msg = new AMQPMessage($message,
                array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
            );
            $channel->basic_publish($msg, '', $queue);
            $channel->close();
            $connection->close();
        }

        return 0;
    }

    

}
