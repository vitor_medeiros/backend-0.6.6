<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;	
use App\Models\Usuario;
use App\Mail\usuarioRecuperarSenha;

class UsuarioControllerRecuperarSenha extends Controller
{
    public function recuperarSenha(Request $request)
    {        
        if ($request->filled('email')){
            $email = $request->email;
        }else{
        	return ['message' => 'The given data was invalid.'];
        }
        try{
            $user = Usuario::where('email', $email)->first();
            if ($user) {
                \Mail::send(new usuarioRecuperarSenha($user)); 
                return ['success' => true];
            }else{
                return ['message' => 'User não encontrado!'];
            }
        }catch(\ModelNotFoundException $ex){
            return ['message' => $ex->getMessage()];
        }
    }

     public function alterarSenha(Request $request)
    {        
        if ($request->filled(['id_usuario', 'token', 'password'])){
            $id_usuario = $request->id_usuario;
            $token = $request->token;
            $password = $request->password;
        }else{
        	return ['message' => 'The given data was invalid.'];
        }
        try{
            $user = Usuario::find($id_usuario);
            if ($user) {
                if ($user->remember_token == $token) {
                	$user->remember_token = null;
                    $user->password = \Hash::make($password);
                    $user->save();
                    return ['success' => true];
                 }else  return ['message' => 'Token inválido!'];
            }else{
                return ['message' => 'User não encontrado!'];
            }
        }catch(\ModelNotFoundException $ex){
            return ['message' => $ex->getMessage()];
        }
    }
}
