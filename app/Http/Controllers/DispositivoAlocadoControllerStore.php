<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DispositivoAlocado;

class DispositivoAlocadoControllerStore extends Controller
{
    public function store(Request $request)
    {
    	if ($request->filled(['cd_dispositivo', 'cd_estacao', 'nome_dispositivo'])) {
	    	$cd_dispositivo = $request->cd_dispositivo;
	    	$cd_estacao = $request->cd_estacao;
	    	$nome_dispositivo = $request->nome_dispositivo;
	    	if ($request->filled('data_hora_inicio_alocacao')) $data_hora_inicio_alocacao = $request->data_hora_inicio_alocacao;
	    	else $data_hora_inicio_alocacao = date("Y-m-d H:i:s");
		}else{
			return ['message' => 'The given data was invalid.'];
		}
        try{
            $consulta = \DB::select("select enum_busca_id('ESTADO_DISPOSITIVO', 8) as cd_estado, enum_busca_id('SINAL', 0) as cd_sinal");
            $cd_sinal = $consulta[0]->cd_sinal;
            $cd_estado = $consulta[0]->cd_estado;
	        try{    
	            $dispositivo_alocado = new DispositivoAlocado;
	            $dispositivo_alocado->cd_dispositivo = $cd_dispositivo;
	            $dispositivo_alocado->cd_estacao = $cd_estacao;
	            $dispositivo_alocado->nome_dispositivo = $nome_dispositivo;
	            $dispositivo_alocado->cd_estado = $cd_estado;
	            $dispositivo_alocado->cd_sinal = $cd_sinal;
	            $dispositivo_alocado->data_hora_inicio_alocacao = $data_hora_inicio_alocacao;
	            $dispositivo_alocado->save();
	            return ['cd_dispositivo' => $cd_dispositivo, 'cd_estacao' => $cd_estacao, 'nome_dispositivo' => $nome_dispositivo,  'cd_estado' => $cd_estado, 'cd_sinal' => $cd_sinal, 'data_hora_inicio_alocacao' => $data_hora_inicio_alocacao, 'created_at' => $dispositivo_alocado->created_at, 'id_dispositivo_alocado' => $dispositivo_alocado->id_dispositivo_alocado];
	        }catch(\ModelNotFoundException $ex){
	        	return ['message' => $ex->getMessage()];
	        }
          
        }catch(\Illuminate\Database\QueryException $ex){
            return ['message' => $ex->getMessage()];
        }
        
        
    }

}
