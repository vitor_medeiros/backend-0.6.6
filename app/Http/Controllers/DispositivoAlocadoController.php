<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\DispositivoAlocadoService;

class DispositivoAlocadoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(DispositivoAlocadoService::class);
    }
}
