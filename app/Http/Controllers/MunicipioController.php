<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\MunicipioService;
use function app;

class MunicipioController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(MunicipioService::class);
    }
}
