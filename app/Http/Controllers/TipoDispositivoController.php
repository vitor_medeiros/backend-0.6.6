<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\TipoDispositivoService;

class TipoDispositivoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(TipoDispositivoService::class);
    }
}
