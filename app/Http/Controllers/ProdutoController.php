<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\ProdutoService;

class ProdutoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(ProdutoService::class);
    }

}
