<?php


namespace App\Http\Controllers;


use App\Services\UsuarioService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UsuarioController
{
    /**
     * @var UsuarioService
     */
    private $service;


    /**
     * UsuarioController constructor.
     * @param UsuarioService $service
     */
    public function __construct(UsuarioService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request): JsonResponse
    {
        $query = $this->service->getAll();
        return response()->json($query, 200);
    }
}
