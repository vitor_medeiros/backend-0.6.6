<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\DispositivoService;

class DispositivoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(DispositivoService::class);
    }
}
