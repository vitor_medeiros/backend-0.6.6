<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\TipoEventoService;

class TipoEventoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(TipoEventoService::class);
    }
}
