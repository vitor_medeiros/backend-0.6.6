<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\EventoService;

class EventoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(EventoService::class);
    }
}
