<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\Models\Dispositivo;
use App\Models\Versao;
use App\Models\VersaoDisponivel;


class VersaoControllerDestroy extends Controller
{
     public function destroy($id)
    {
        try{
            $versao = Versao::find($id);
            $versao_disponivel = VersaoDisponivel::where('cd_versao', $id)->first();
            $dispositivo = Dispositivo::where('cd_versao', $id)->first();
            if ($versao_disponivel) {
                return ['message' => 'Versão usada em tabelas dependentes!'];
            }else if ($dispositivo) {
                return ['message' => 'Versão usada em tabelas dependentes!'];  
            }else{
                if($versao){
                    $versao->delete();
                    return ['success' => true];
                }else{
                    return ['message' => 'Wrong ID!'];
                }
            }
        }catch(\ModelNotFoundException $ex){
            return ['message' => $ex->getMessage()];
        }
        
        
    }
}
