<?php

namespace App\Http\Controllers;

use App\Core\AbstractApiController;
use App\Core\AbstractService;
use App\Services\EstacaoService;

class EstacaoController extends AbstractApiController
{

    public function getService(): AbstractService
    {
        return app(EstacaoService::class);
    }
}
