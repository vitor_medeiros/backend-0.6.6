<?php


namespace App\Core;

use App\Core\Interfaces\AbstractModelInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class AbstractModel extends Model implements AbstractModelInterface
{
    use SoftDeletes;
}
