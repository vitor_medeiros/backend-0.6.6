<?php


namespace App\Core;

use App\Core\Interfaces\AbstractServiceInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

abstract class AbstractService implements AbstractServiceInterface
{
    public function getAll(): LengthAwarePaginator
    {
        $query = $this->getModelClass()::query();
        $query = $query->paginate();
        return $query;
    }

    public function getOne($id): Model
    {
        return $this->getModelClass()::query()->findOrFail($id);
    }

    public function save($data): Model
    {
        $model = $this->getModelClass();
        $model->fill($data);
        try {
            DB::beginTransaction();
            $model->save();
            DB::commit();
            return $model;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    public function update($data, $id): Model
    {
        $model = $this->getModelClass()::query()->findOrFail($id);
        try {
            DB::beginTransaction();
            $model->fill($data);
            $model->save();
            DB::commit();
            return $model;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }

    public function destroy($id): array
    {
        $model = $this->getModelClass()::query()->findOrFail($id);
        try {
            DB::beginTransaction();
            $return = ['success' => $model->delete()];
            DB::commit();
            return $return;
        } catch (Throwable $throwable) {
            DB::rollBack();
            throw $throwable;
        }
    }
}
