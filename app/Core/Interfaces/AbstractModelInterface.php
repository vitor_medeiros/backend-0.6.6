<?php


namespace App\Core\Interfaces;

interface AbstractModelInterface
{
    public function getCreateRules(): array;

    public function getUpdateRules(): array;
}
