<?php

namespace App\Core\Interfaces;

/**
 * Handle event tag service pattern
 */
interface EventTagService {

    /**
     * Define the message event type
     */
    function getEvent(): string;

    /**
     * Process the message
     * 
     * @param TagQueueMessage $message
     */
    function evaluate(TagQueueMessage $message);
}
?>