<?php

namespace App\Core\Interfaces;

/**
 * Tag Queue Message model
 */
interface TagQueueMessage
{
    /**
     * Get the message event source type
     * @return string
     */
    public function getEvent(): string;

    /**
     * Access content data
     * @return QueueMessageParser
     */
    function getContent(): QueueMessageParser;
    

    /**
     * Access station data. This data is optional
     * @return QueueMessageParser
     */
    function getStation(): QueueMessageParser;
}
?>