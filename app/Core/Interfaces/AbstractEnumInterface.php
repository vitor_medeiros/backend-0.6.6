<?php


namespace App\Core\Interfaces;

interface AbstractEnumInterface
{
    public static function allowedValues(): string;
    public static function toInValidateString(): string;
}
