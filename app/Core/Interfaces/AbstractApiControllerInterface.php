<?php


namespace App\Core\Interfaces;

use App\Core\AbstractService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

interface AbstractApiControllerInterface
{
    public function getService(): AbstractService;

    public function index(Request $request): JsonResponse;

    public function show($id): JsonResponse;

    public function store(Request $request): JsonResponse;

    public function update(Request $request, $id): JsonResponse;

    public function destroy($id): JsonResponse;
}
