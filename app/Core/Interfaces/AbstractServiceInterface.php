<?php


namespace App\Core\Interfaces;

use App\Core\AbstractModel;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

interface AbstractServiceInterface
{
    public function getModelClass(): AbstractModel;

    public function getAll(): LengthAwarePaginator;

    public function getOne($id): Model;

    public function save($data): Model;

    public function update($data, $id): Model;

    public function destroy($id): array;
}
