<?php

namespace App\Core\Interfaces;

/**
 * Handle the queue message 
 */
interface QueueMessageHandler {
    /**
     * Processes the queue message body
     */
    public function handle(string $rawMessage);
}
?>