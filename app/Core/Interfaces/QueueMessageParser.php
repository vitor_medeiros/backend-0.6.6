<?php
namespace App\Core\Interfaces;

interface QueueMessageParser
{
     /**
     * Get parameter based on the $key
     * @param $key the key to access the content
     * 
     * @return mixed
     */
    function getParam($key);
    

     /**
     * Get parameter as DateTime format based on the $key
     * @param $key the key to access the content
     * 
    * @throws InvalidFormatException
     * 
     * @return string
     */
    function getDateTimeParam($key): string;
}

?>