<?php


namespace App\Core;

use App\Core\Interfaces\AbstractApiControllerInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Throwable;

abstract class AbstractApiController extends Controller implements AbstractApiControllerInterface
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $query = $this->getService()->getAll();
        return response()->json($query, 200);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $model = $this->getService()->getOne($id);
        return response()->json($model, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $validated = $this->validate($request, $this->getService()->getModelClass()->getCreateRules());
        $model = $this->getService()->save($validated);
        return response()->json($model, 201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws Throwable
     * @throws ValidationException
     */
    public function update(Request $request, $id): JsonResponse
    {
        $validated = $this->validate($request, $this->getService()->getModelClass()->getCreateRules());
        $model = $this->getService()->update($validated, $id);
        return response()->json($model, 201);
    }

    /**
     * @param $id
     * @return JsonResponse
     * @throws Throwable
     */
    public function destroy($id): JsonResponse
    {
        $return = $this->getService()->destroy($id);
        return response()->json($return, 200);
    }
}
