<?php

namespace App\Models;

use App\Core\AbstractModel;

class TipoDispositivo extends AbstractModel
{
    protected $table = 'tipo_dispositivo';
    protected $primaryKey = 'id_tipo_dispositivo';

    protected $fillable = [
        'id_tipo_dispositivo',
        'descricao',
        'funcao',
        'informacoes',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_tipo_dispositivo' => 'integer',
        'descricao' => 'string',
        'funcao' => 'string',
        'informacoes' => 'string',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'descricao' => 'required|string|max:50',
            'informacoes' => 'nullable|string',
            'funcao' => 'nullable|string',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'descricao' => 'string|max:50',
            'informacoes' => 'nullable|string',
            'funcao' => 'nullable|string',
        ];
    }
}
