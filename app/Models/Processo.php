<?php

namespace App\Models;

use App\Core\AbstractModel;

class Processo extends AbstractModel
{
    protected $table = 'processo';
    protected $primaryKey = 'id_processo';

    protected $fillable = [
        'id_processo',
        'cd_produto',
        'cd_dispositivo_alocado',
        'data_hora_liberacao',
        'data_hora_inicio',
        'data_hora_fim',
        'segundos_pausas',
        'pausas',
        'violado',
        'litro_agua',
        'vazao_agua_media',
        'energia_kw_h',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_processo' => 'integer',
        'cd_produto' => 'integer',
        'cd_dispositivo_alocado' => 'integer',
        'data_hora_liberacao' => 'date',
        'data_hora_inicio' => 'date',
        'data_hora_fim' => 'date',
        'segundos_pausas' => 'integer',
        'pausas' => 'integer',
        'violado' => 'boolean',
        'litro_agua' => 'integer',
        'vazao_agua_media' => 'decimal:3',
        'energia_kw_h' => 'decimal:3',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'data_hora_liberacao',
        'data_hora_inicio',
        'data_hora_fim',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'cd_produto' => 'required|integer|exists:produto,id_produto',
            'cd_dispositivo_alocado' => 'required|integer|exists:dispositivo_alocado,id_dispositivo_alocado',
            'data_hora_liberacao' => 'required|date',
            'data_hora_inicio' => 'nullable|date',
            'data_hora_fim' => 'nullable|date',
            'segundos_pausas' => 'nullable|integer',
            'pausas' => 'nullable|integer',
            'violado' => 'required|boolean',
            'litro_agua' => 'nullable|integer',
            'vazao_agua_media' => 'nullable|double',
            'energia_kw_h' => 'nullable|double'
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'cd_produto' => 'integer|exists:produto,id_produto',
            'cd_dispositivo_alocado' => 'integer|exists:dispositivo_alocado,id_dispositivo_alocado',
            'data_hora_liberacao' => 'date',
            'data_hora_inicio' => 'date',
            'data_hora_fim' => 'date',
            'segundos_pausas' => 'integer',
            'pausas' => 'integer',
            'violado' => 'boolean',
            'litro_agua' => 'integer',
            'vazao_agua_media' => 'double',
            'energia_kw_h' => 'double'
        ];
    }
}
