<?php

namespace App\Models;

use App\Core\AbstractModel;

class UsuarioEstacao extends AbstractModel
{
    protected $table = 'usuario_estacao';
    protected $primaryKey = 'id_usuario_estacao';

    protected $fillable = [
        'id_usuario_estacao',
        'cd_estacao',
        'cd_usuario',
        'vende',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_usuario_estacao' => 'integer',
        'cd_estacao' => 'integer',
        'cd_usuario' => 'integer',
        'vende' => 'boolean',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        
        return [
            'cd_estacao' => 'required|integer|exists:estacao,id_estacao',
            'cd_usuario' => 'required|integer|exists:usuario,id_usuario',
            'vende' => 'required|boolean',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'cd_estacao' => 'integer|exists:estacao,id_estacao',
            'cd_usuario' => 'integer|exists:usuario,id_usuario',
            'vende' => 'required|boolean',
        ];
    }
}
