<?php

namespace App\Models;

use App\Core\Interfaces\AbstractModelInterface;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Usuario extends Authenticatable implements JWTSubject, AbstractModelInterface
{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'usuario';
    protected $primaryKey = 'id_usuario';

    protected $fillable = [
        'id_usuario',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'nome_usuario',
        'tipo_perfil',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'id_usuario' => 'integer',
        'email' => 'string',
        'email_verified_at' => 'date',
        'password' => 'string',
        'remember_token' => 'string',
        'nome_usuario' => 'string',
        'tipo_perfil' => 'string',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'email_verified_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey() . '';
    }

    public function getJWTCustomClaims()
    {
        return [
            "admin" => true,
            "https://hasura.io/jwt/claims" => [
                "x-hasura-allowed-roles" => ['admin'],
//                "x-hasura-allowed-roles" => $this->grupos->toArray() ?? [],
                "x-hasura-default-role" => "admin",
                "x-hasura-user-id" => $this->id_usuario . "",
            ]
        ];
    }

    public function grupos()
    {
        return $this->belongsToMany(
            Grupo::class,
            UsuarioGrupo::class,
            'cd_usuario',
            'cd_grupo'
        );
    }

    public function getCreateRules(): array
    {
        return [
            'nome' => 'nullable|string',
            'email' => 'nullable|string|email|max:255|unique:usuario',
            'password' => 'required|string|min:6|confirmed',
            'nome_usuario' => 'nullable|string',
            'tipo_perfil' => 'string|max:1|in:A,O,G,V',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'nome' => 'nullable|string',
            'email' => 'nullable|string|email|max:255|unique:usuario',
            'password' => 'required|string|min:6|confirmed',
            'nome_usuario' => 'nullable|string',
            'tipo_perfil' => 'string|max:1|in:A,O,G,V',
        ];
    }
}
