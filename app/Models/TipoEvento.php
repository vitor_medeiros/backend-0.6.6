<?php

namespace App\Models;

use App\Core\AbstractModel;

class TipoEvento extends AbstractModel
{
    protected $table = 'tipo_evento';
    protected $primaryKey = 'id_tipo_evento';

    protected $fillable = [
        'id_tipo_evento',
        'evento',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_tipo_evento' => 'integer',
        'evento' => 'string',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'evento' => 'required|string|max:20'
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'evento' => 'required|string|max:20'
        ];
    }
}
