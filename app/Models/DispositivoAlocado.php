<?php

namespace App\Models;

use App\Core\AbstractModel;

class DispositivoAlocado extends AbstractModel
{
    protected $table = 'dispositivo_alocado';
    protected $primaryKey = 'id_dispositivo_alocado';

    protected $fillable = [
        'id_dispositivo_alocado',
        'cd_dispositivo',
        'cd_estacao',
        'cd_estado',
        'cd_sinal',
        'data_hora_inicio_alocacao',
        'data_hora_fim_alocacao',
        'numero_chuveiro',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_dispositivo_alocado' => 'integer',
        'cd_dispositivo' => 'integer',
        'cd_estacao' => 'integer',
        'cd_estado' => 'integer',
        'cd_sinal',
        'data_hora_inicio_alocacao' => 'date',
        'data_hora_fim_alocacao' => 'date',
        'numero_chuveiro' => 'string',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'data_hora_inicio_alocacao',
        'data_hora_fim_alocacao',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'cd_dispositivo' => 'required|integer|exists:dispositivo,id_dispositivo',
            'cd_estacao' => 'required|integer|exists:estacao,id_estacao',
            'cd_estado' => 'required|integer|exists:enum,id_enum,entidade,ESTADO_DISPOSITIVO',
            'cd_sinal' => 'required|integer|exists:enum,id_enum,entidade,SINAL',
            'nome_dipositivo' => 'required|string|max:20',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'cd_dispositivo' => 'integer|exists:dispositivo,id_dispositivo',
            'cd_estacao' => 'integer|exists:estacao,id_estacao',
            'cd_estado' => 'integer|exists:enum,id_enum,entidade,ESTADO_DISPOSITIVO',
            'cd_sinal' => 'integer|exists:enum,id_enum,entidade,SINAL',
            'data_hora_inicio_alocacao' => 'date',
            'data_hora_fim_alocacao' => 'date',
            'nome_dipositivo' => 'string|max:20',
        ];
    }
}
