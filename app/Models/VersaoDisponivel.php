<?php

namespace App\Models;

use App\Core\AbstractModel;

class VersaoDisponivel extends AbstractModel
{

    protected $table = 'versao_disponivel';
    protected $primaryKey = 'id_versao_disponivel';

    protected $fillable = [
        'id_versao_disponivel',
        'cd_estacao',
        'cd_versao',
        'data_liberacao',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_versao_disponivel' => 'integer',
        'cd_estacao' => 'integer',
        'cd_versao' => 'integer',
        'data_liberacao' => 'date',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'data_liberacao',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'id_versao_disponivel' => 'required|integer',
            'cd_estacao' => 'required|integer|exists:estacao,id_estacao',
            'cd_versao' => 'required|integer|exists:versao,id_versao',
            'data_liberacao' => 'required|date',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'id_versao_disponivel' => 'integer',
            'cd_estacao' => 'integer|exists:estacao,id_estacao',
            'cd_versao' => 'integer|exists:versao,id_versao',
            'data_liberacao' => 'date',
        ];
    }
}
