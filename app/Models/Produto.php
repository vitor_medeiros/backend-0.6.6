<?php

namespace App\Models;

use App\Core\AbstractModel;

class Produto extends AbstractModel
{
    protected $table = 'produto';
    protected $primaryKey = 'id_produto';

    protected $fillable = [
        'id_produto',
        'cd_estacao',
        'codigo',
        'descricao',
        'desc_reduzida',
        'segundos',
        'pausas',
        'segundos_pausa',
        'valor',
        'e_cortesia',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [
        'id_produto' => 'integer',
        'cd_estacao' => 'integer',
        'codigo' => 'integer',
        'descricao' => 'string',
        'desc_reduzida' => 'string',
        'segundos' => 'integer',
        'pausas' => 'integer',
        'segundos_pausa' => 'integer',
        'valor' => 'decimal:2',
        'e_cortesia' => 'boolean',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $casts = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'cd_estacao' => 'required|integer|exists:estacao,id_estacao',
            'codigo' => 'nullable|integer',
            'descricao' => 'required|string|max:40',
            'desc_reduzida' => 'nullable|string|max:20',
            'segundos' => 'required|integer',
            'pausas' => 'nullable|integer',
            'segundos_pausa' => 'nullable|integer',
            'valor' => 'required|double',
            'e_cortesia' => 'required|boolean',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'cd_estacao' => 'integer|exists:estacao,id_estacao',
            'codigo' => 'integer',
            'descricao' => 'string|max:40',
            'desc_reduzida' => 'string|max:20',
            'segundos' => 'integer',
            'pausas' => 'integer',
            'segundos_pausa' => 'integer',
            'valor' => 'double',
            'e_cortesia' => 'boolean',
        ];
    }
}
