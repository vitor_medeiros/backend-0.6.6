<?php

namespace App\Models;

use App\Core\AbstractModel;

class Municipio extends AbstractModel
{
    protected $table = 'municipio';
    protected $primaryKey = 'id_municipio';

    protected $fillable = [
        'id_municipio',
        'municipio',
        'uf',
        'ibge',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_municipio' => 'integer',
        'municipio' => 'string',
        'ibge' => 'string',
        'uf' => 'string',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'municipio' => 'required|string',
            'ibge' => 'required|string|unique:municipio',
            'uf' => 'required|string|max:2'
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'municipio' => 'string',
            'ibge' => 'string',//todo colocar o unique tirando o registro atual
            'uf' => 'string|max:2'
        ];
    }
}
