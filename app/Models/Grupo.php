<?php

namespace App\Models;

use App\Core\AbstractModel;

class Grupo extends AbstractModel
{
    protected $table = 'grupo';
    protected $primaryKey = 'id_grupo';

    protected $fillable = [
        'id_grupo',
        'nome',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_grupo' => 'integer',
        'nome' => 'string',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'nome' => 'required|string:unique:grupo,nome',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'nome' => 'string:unique:grupo,nome',
        ];
    }
}
