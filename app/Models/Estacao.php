<?php

namespace App\Models;

use App\Core\AbstractModel;

class Estacao extends AbstractModel
{
    protected $table = 'estacao';
    protected $primaryKey = 'id_estacao';

    protected $fillable = [
        'id_estacao',
        'cd_tipo_contrato',
        'cd_situacao',
        'cd_municipio',
        'codigo',
        'nome',
        'maquinas_liberadas',
        'cnpj_empresa',
        'nome_empresa',
        'dia_fechamento',
        'dia_vencimento',
        'data_periodo_atual',
        'data_ultimo_periodo_pago',
        'latitude',
        'longitude',
        'observacoes',
        'estado',
        'data_ultimo_contrato',
        'contrato',
        'valor_por_unidade',
        'nome_rede',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_estacao' => 'integer',
        'cd_municipio' => 'integer',
        'cd_tipo_contrato' => 'integer',
        'cd_situacao' => 'integer',
        'codigo' => 'string',
        'nome' => 'string',
        'maquinas_liberadas' => 'integer',
        'cnpj_empresa' => 'string',
        'nome_empresa' => 'string',
        'dia_fechamento' => 'integer',
        'dia_vencimento' => 'integer',
        'data_periodo_atual' => 'date',
        'data_ultimo_periodo_pago' => 'date',
        'latitude' => 'double',
        'longitude' => 'double',
        'observacoes' => 'string',
        'estado' => 'integer',
        'data_ultimo_contrato' => 'date',
        'contrato' => 'integer',
        'valor_por_unidade' => 'decimal:3',
        'nome_rede' => 'string',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'data_periodo_atual',
        'data_ultimo_periodo_pago',
        'data_ultimo_contrato',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'cd_municipio' => 'integer|exists:municipio,id_municipio',
            'cd_situacao' => 'required|integer|exists:enum,id_enum,entidade,SITUACAO_ESTACAO',
            'cd_tipo_contrato' => 'required|integer|exists:enum,id_enum,entidade,TIPO_CONTRATO',
            'codigo' => 'nullable|string|max:10|unique:estacao,codigo',
            'nome' => 'required|string|max:100',
            'maquinas_liberadas' => 'required|integer',
            'cnpj_empresa' => 'nullable|string|max:20',
            'nome_empresa' => 'nullable|string|max:100',
            'dia_fechamento' => 'required|integer',
            'dia_vencimento' => 'required|integer',
            'data_periodo_atual' => 'required|date',
            'data_ultimo_periodo_pago' => 'nullable|date',
            'latitude' => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
            'observacoes' => 'nullable|string',
            'data_ultimo_contrato' => 'nullable|date',
            'valor_por_unidade' => 'required|numeric',
            'nome_rede' => 'nullable|string|max:100',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'cd_municipio' => 'exists:municipio,id_municipio',
            'cd_situacao' => 'required|integer|exists:enum,id_enum,entidade,SITUACAO_ESTACAO',
            'cd_tipo_contrato' => 'required|integer|exists:enum,id_enum,entidade,TIPO_CONTRATO',
            'codigo' => 'string|max:10',
            'nome' => 'string|max:100',
            'maquinas_liberadas' => 'integer',
            'cnpj_empresa' => 'string|max:20',
            'nome_empresa' => 'string|max:100',
            'dia_fechamento' => 'integer',
            'dia_vencimento' => 'integer',
            'data_periodo_atual' => 'date',
            'data_ultimo_periodo_pago' => 'date',
            'latitude' => 'numeric',
            'longitude' => 'numeric',
            'observacoes' => 'string',
            'data_ultimo_contrato' => 'date',
            'valor_por_unidade' => 'numeric',
            'nome_rede' => 'string|max:100',
        ];
    }
}
