<?php

namespace App\Models;

use App\Core\AbstractModel;

class Enum extends AbstractModel
{
    protected $table = 'enum';
    protected $primaryKey = 'id_enum';

    protected $fillable = [
        'entidade',
        'codigo',
        'descricao',
        'observacao',
    ];

    protected $hidden = [];

    protected $casts = [
        'descricao' => 'string',
        'entidade' => 'string',
        'codigo' => 'integer',
        'observacao' => 'string',
    ];

    protected $dates = [];

    public function getCreateRules(): array
    {
        return [
            'descricao' => 'required|string|unique:enum,descricao',
            'entidade' => 'required|string',
            'codigo' => 'required|integer',
            'observacao' => 'required|string',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'descricao' => 'string|unique:enum,descricao',//todo retirar atual
            'entidade' => 'string',
            'codigo' => 'integer',
            'observacao' => 'string',
        ];
    }
}
