<?php

namespace App\Models;

use App\Core\AbstractModel;

class Dispositivo extends AbstractModel
{
    protected $table = 'dispositivo';
    protected $primaryKey = 'id_dispositivo';

    protected $fillable = [
        'id_dispositivo',
        'cd_tipo_dispositivo',
        'cd_versao',
        'numero_serie',
        'modelo',
        'lote',
        'data_producao',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_dispositivo' => 'integer',
        'cd_tipo_dispositivo' => 'integer',
        'cd_versao' => 'integer',
        'numero_serie' => 'string',
        'modelo' => 'string',
        'lote' => 'string',
        'data_producao' => 'date',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'data_producao',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'cd_tipo_dispositivo' => 'required|integer|exists:tipo_dispositivo,id_tipo_dispositivo',
            'cd_versao' => 'required|integer|exists:versao,id_versao',
            'numero_serie' => 'required|string|max:20',
            'modelo' => 'nullable|string|max:20',
            'lote' => 'nullable|string|max:20',
            'data_producao' => 'nullable|date'
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'cd_tipo_dispositivo' => 'integer|exists:produto,id_produto',
            'numero_serie' => 'string|max:20',
            'modelo' => 'nullable|string|max:20',
            'lote' => 'nullable|string|max:20',
            'data_producao' => 'date'
        ];
    }

}
