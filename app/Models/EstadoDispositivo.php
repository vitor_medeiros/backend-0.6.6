<?php

namespace App\Models;

use App\Core\AbstractModel;

class EstadoDispositivo extends AbstractModel
{
    protected $table = 'estado_dispositivo';
    protected $primaryKey = 'id_estado_dispositivo';

    protected $fillable = [
        'id_estado_dispositivo',
        'estado',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_estado_dispositivo' => 'integer',
        'estado' => 'string',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'estado' => 'required|string|max:20'
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'estado' => 'string|max:20'
        ];
    }
}
