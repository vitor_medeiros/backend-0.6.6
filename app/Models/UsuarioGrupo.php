<?php

namespace App\Models;

use App\Core\AbstractModel;

class UsuarioGrupo extends AbstractModel
{
    protected $table = 'usuario_grupo';
    protected $primaryKey = 'id_usuario_grupo';

    protected $fillable = [
        'id_usuario_grupo',
        'cd_grupo',
        'cd_usuario',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_usuario_grupo' => 'integer',
        'cd_grupo' => 'integer',
        'cd_usuario' => 'integer',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'cd_usuario' => 'required|string:exists:usuario,id_usuario',
            'cd_grupo' => 'required|string:exists:grupo,cd_grupo',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'cd_usuario' => 'string:exists:usuario,id_usuario',
            'cd_grupo' => 'string:exists:grupo,cd_grupo',
        ];
    }
}
