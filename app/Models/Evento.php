<?php

namespace App\Models;

use App\Core\AbstractModel;

class Evento extends AbstractModel
{
    protected $table = 'evento';
    protected $primaryKey = 'id_evento';

    protected $fillable = [
        'id_evento',
        'cd_dispositivo_alocado',
        'cd_tipo_evento',
        'data_hora',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_evento' => 'integer',
        'cd_dispositivo_alocado' => 'integer',
        'cd_tipo_evento' => 'integer',
        'data_hora' => 'date',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'data_hora',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'cd_dispositivo_alocado' => 'required|integer|exists:dispositivo_alocado,id_dispositivo_alocado',
            'cd_tipo_evento' => 'required|integer|exists:enum,id_enum,entidade,TIPO_EVENTO',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'cd_dispositivo_alocado' => 'integer|exists:dispositivo_alocado,id_dispositivo_alocado',
            'cd_tipo_evento' => 'integer|exists:enum,id_enum,entidade,TIPO_EVENTO'
        ];
    }

}
