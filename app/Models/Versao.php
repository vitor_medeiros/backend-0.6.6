<?php

namespace App\Models;

use App\Core\AbstractModel;

class Versao extends AbstractModel
{
    protected $table = 'versao';
    protected $primaryKey = 'id_versao';

    protected $fillable = [
        'id_versao',
        'cd_tipo_dispositivo',
        'notas',
        'caminho_arquivo',
        'data_liberacao',
        'validada_producao',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [];

    protected $casts = [
        'id_versao' => 'integer',
        'cd_tipo_dispositivo' => 'integer',
        'notas' => 'nullable|string',
        'caminho_arquivo' => 'nullable|string',
        'data_liberacao' => 'date',
        'validada_producao' => 'boolean',
        'created_at' => 'date',
        'updated_at' => 'date',
        'deleted_at' => 'date'
    ];

    protected $dates = [
        'data_liberacao',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getCreateRules(): array
    {
        return [
            'cd_tipo_dispositivo' => 'required|integer:exists:tipo_dispositivo,id_tipo_dispositivo',
            'notas' => 'required|string',
            'caminho_arquivo' => 'required|string|max:255',
            'data_liberacao' => 'required|date',
            'validada_producao' => 'required|boolean',
        ];
    }

    public function getUpdateRules(): array
    {
        return [
            'cd_tipo_dispositivo' => 'integer:exists:tipo_dispositivo,id_tipo_dispositivo',
            'notas' => 'string',
            'caminho_arquivo' => 'string|max:255',
            'data_liberacao' => 'date',
            'validada_producao' => 'boolean',
        ];
    }
}
