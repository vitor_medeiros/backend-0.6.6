<?php

namespace App\Console\Commands;
use Carbon\Carbon;
use \Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\Console\Commands\Consumers\EstadoConsumerClass;
use App\Console\Commands\Consumers\EventoConsumerClass;
use App\Console\Commands\Consumers\ProcessoConsumerClass;
use App\Console\Commands\Consumers\TipoDispositivoConsumerClass;
use App\Console\Commands\Consumers\HubSincronizadoConsumerClass;
use App\Console\Commands\Consumers\HubReinicioConsumerClass;


class EventoConsumer extends Command{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'evento:consumer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consume os eventos em geral';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    //handle();
    public function handle()
    {
        function conecta(){
            $connection = new AMQPStreamConnection('amqp.sis.eletromais.tec.br', 5672, 'rabbitmq', 'rabbitmq');
            $channel = $connection->channel();
            $channel->basic_consume('api_geral', '', false, true, false, false, function($message){
                $obj = json_decode($message->body, true);
                $tipo_mensagem = $obj['evento'];  

                if ($tipo_mensagem=='evento') {
                    $evento = new EventoConsumerClass();
                    $evento->getMessage($obj);
                     
                }elseif ($tipo_mensagem=='processo') {
                    $evento = new processoConsumerClass();
                    $evento->getMessage($obj);
                     
                }elseif ($tipo_mensagem=='tipo_dispositivo') {
                    $evento = new TipoDispositivoConsumerClass();
                    $evento->getMessage($obj);                     

                }elseif ($tipo_mensagem=='hub_sincronizado') {
                    $evento = new HubSincronizadoConsumerClass();
                    $evento->getMessage($obj);    

                }elseif ($tipo_mensagem=='hub_reinicio') {
                    $evento = new HubReinicioConsumerClass();
                    $evento->getMessage($obj);
                }


            });
            $contador = 0;
            while ($channel->is_consuming()) {
                $channel->wait();
               
                if ($contador==1000) {
                    $channel->close();
                    $connection->close();
                    unset($channel);
                    unset($connection);
                    $contador = 0;
                    conecta();
                }else $contador = $contador + 1;
                
            }
        }
        conecta();
        //$channel->close();
        //$connection->close();
        return 0;
    }
}
