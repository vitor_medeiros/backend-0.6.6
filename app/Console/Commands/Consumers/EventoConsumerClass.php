<?php

namespace App\Console\Commands\Consumers;

use Carbon\Carbon;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class EventoConsumerClass{

    public function getMessage($obj) 
    {
        $numero_serie = $obj['dados']['numero_serie'];
        $cd_tipo_evento = $obj['dados']['codigo'];
        $data_hora = $obj['dados']['data_hora_recebimento'];
        $tipo_mensagem = $obj['evento'];
        try{
            $consulta = \DB::select("select id_dispositivo_alocado from dispositivo_alocado join dispositivo on id_dispositivo = cd_dispositivo
                where numero_serie = '$numero_serie' and dispositivo_alocado.deleted_at IS NULL");
            $cd_dispositivo_alocado = $consulta[0]->id_dispositivo_alocado;

            $consulta1 = \DB::insert("insert into evento (cd_dispositivo_alocado, cd_tipo_evento, data_hora, created_at, updated_at) values($cd_dispositivo_alocado, enum_busca_id('TIPO_EVENTO', $cd_tipo_evento), '$data_hora', NOW(), NOW())");
    
        }catch(\Illuminate\Database\QueryException $ex){
            //ver com a equipe oq fazer em caso de erro
            $mensagem = json_encode($obj);
            $erro  =$ex->getMessage();
            $erro = str_replace(array('\''), '', $erro); 
            $consultax = \DB::insert("insert into logs_integracao (erro_integracao, mensagem, data_hora_mensagem, data_hora_integracao, tipo_mensagem, numero_serie, created_at, updated_at) values ('$erro', '$mensagem', '$data_hora', NOW(), '$tipo_mensagem', '$numero_serie', NOW(), NOW())"); //em caso de erro cria log
            //echo $ex->getMessage();
        }
    }
}
