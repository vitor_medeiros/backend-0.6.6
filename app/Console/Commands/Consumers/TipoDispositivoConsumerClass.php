<?php

namespace App\Console\Commands\Consumers;

use Carbon\Carbon;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class TipoDispositivoConsumerClass{

    public function getMessage($obj) 
    {
        $numero_serie = $obj['dados']['numero_serie'];
        $data_hora = $obj['dados']['data_hora_recebimento'];
        $cd_estacao = $obj['estacao']['codigo_estacao']; //estacao da mensagem
        $cd_sinal = $obj['dados']['rssi'];
        $modo = $obj['dados']['modo'];
        $versao = $obj['dados']['versao'];
        $tipo_mensagem = $obj['evento'];
        try{
            $consulta = \DB::select("select id_dispositivo_alocado, id_dispositivo, cd_estacao, cd_tipo_dispositivo, cd_versao from dispositivo_alocado RIGHT join dispositivo on id_dispositivo = cd_dispositivo where dispositivo.numero_serie = '$numero_serie' and dispositivo_alocado.deleted_at IS NULL"); //usei right join, pois pode haver dispositivo, mas ele não está alocado.
            if ($consulta) { //verifica se tem dispositivo mesmo não sendo alocado
                //echo "dispositivo existe \n";
                $id_dispositivo = $consulta[0]->id_dispositivo;
                $cd_versao = $consulta[0]->cd_versao;
                $consulta_versao = \DB::select("select id_versao from versao where notas = '$versao'");
                if($consulta_versao) $versao = $consulta_versao[0]->id_versao;
                else $versao = 32;

                if ($versao!=$cd_versao) {
                    $consulta_versao1 = \DB::update("update dispositivo set cd_versao = $versao, updated_at = NOW() where id_dispositivo = $id_dispositivo"); //atualiza versao
                }

                if ($consulta[0]->id_dispositivo_alocado) { //se dispositivo for alocado
                    //echo "dispositivo alocado \n";
                    $id_dispositivo_alocado = $consulta[0]->id_dispositivo_alocado;
                    $cd_estacao_dispositivo = $consulta[0]->cd_estacao; //estacao do  dispositivo alocado
                    $cd_tipo_dispositivo = $consulta[0]->cd_tipo_dispositivo;
                    
                    $consulta1 = \DB::insert("insert into evento (cd_dispositivo_alocado, cd_tipo_evento, data_hora, created_at, updated_at) values($id_dispositivo_alocado, enum_busca_id('TIPO_EVENTO', 2), '$data_hora', NOW(), NOW())"); //insert na tabela evento

                    if (!$cd_estacao_dispositivo==$cd_estacao) {
                        $consulta2 = \DB::update("update dispositivo_alocado set cd_estado = enum_busca_id('ESTADO_DISPOSITIVO', 1), updated_at = NOW() where id_dispositivo_alocado = $id_dispositivo_alocado"); 
                    }else{//se tiver na estação certa ele verifica o tipo
                        if ($modo=="C" and $cd_tipo_dispositivo == 3) {
                            $consulta3 = \DB::update("update dispositivo set cd_tipo_dispositivo = 2, updated_at = NOW() where id_dispositivo = $id_dispositivo");
                        }elseif($modo=="M" and $cd_tipo_dispositivo == 2){
                            $consulta4 = \DB::update("update dispositivo set cd_tipo_dispositivo = 3, updated_at = NOW() where id_dispositivo = $id_dispositivo");
                        }
                    }
                }else{ //se nao for alocado
                    //echo "dispositivo não ALOCADO \n";       
                    $consulta2 = \DB::insert("insert into dispositivo_alocado (cd_dispositivo, cd_estacao, cd_estado, cd_sinal, data_hora_inicio_alocacao, nome_dispositivo, data_hora_ultima_comunicacao, created_at, updated_at) values($id_dispositivo, $cd_estacao, enum_busca_id('ESTADO_DISPOSITIVO', 1), enum_busca_id('SINAL', $cd_sinal), NOW(), '$numero_serie', '$data_hora', NOW(), NOW())"); //insere em dispositivo alocado

                    $consulta3 = \DB::select("select id_dispositivo_alocado, cd_tipo_dispositivo from dispositivo_alocado join dispositivo on cd_dispositivo = id_dispositivo  where cd_dispositivo = $id_dispositivo"); //id do dispositivo alocado na ultima consulta
                    $id_dispositivo_alocado = $consulta3[0]->id_dispositivo_alocado;
                    $cd_tipo_dispositivo = $consulta3[0]->cd_tipo_dispositivo;

                    $consulta4 = \DB::insert("insert into evento (cd_dispositivo_alocado, cd_tipo_evento, data_hora, created_at, updated_at) values($id_dispositivo_alocado, enum_busca_id('TIPO_EVENTO', 2), '$data_hora', NOW(), NOW())"); //insert na tabela evento

                    if ($modo=="C" and $cd_tipo_dispositivo == 3) {
                        $consulta5 = \DB::update("update dispositivo set cd_tipo_dispositivo = 2, updated_at = NOW() where id_dispositivo = $id_dispositivo");
                    }elseif($modo=="M" and $cd_tipo_dispositivo == 2){
                        $consulta6 = \DB::update("update dispositivo set cd_tipo_dispositivo = 3, updated_at = NOW() where id_dispositivo = $id_dispositivo");
                    }
                }
            }else{ //insere dispositivo se nao existir
                //echo "dispositivo não existe \n";
                $cd_tipo_dispositivo = 1;
                if ($modo=="C") $cd_tipo_dispositivo = 2;  
                elseif($modo=="M") $cd_tipo_dispositivo = 3;
                $modelo = 'Auto cadastro ' . date('d/m/Y H:m:s');
                
                $consulta_versao = \DB::select("select id_versao from versao where notas = '$versao'");
                $versao = $consulta_versao[0]->id_versao;

                $consulta2 = \DB::insert("insert into dispositivo (cd_versao, cd_tipo_dispositivo, numero_serie, modelo, lote, created_at, updated_at) values ($versao, $cd_tipo_dispositivo, '$numero_serie', '$modelo', '', NOW(), NOW())");

                $consulta3 = \DB::select("select id_dispositivo from dispositivo where numero_serie = '$numero_serie'");
                $id_dispositivo = $consulta3[0]->id_dispositivo;

                $consulta4 = \DB::insert("insert into dispositivo_alocado (cd_dispositivo, cd_estacao, cd_estado, cd_sinal, data_hora_inicio_alocacao, nome_dispositivo, data_hora_ultima_comunicacao, created_at, updated_at) values($id_dispositivo, $cd_estacao, enum_busca_id('ESTADO_DISPOSITIVO', 1), enum_busca_id('SINAL', $cd_sinal), NOW(), '$numero_serie', '$data_hora', NOW(), NOW())"); //insere em dispositivo alocado

                $consulta5 = \DB::select("select id_dispositivo_alocado from dispositivo_alocado where cd_dispositivo = $id_dispositivo"); //id do dispositivo alocado na ultima consulta
                    $id_dispositivo_alocado = $consulta5[0]->id_dispositivo_alocado;

                $consulta6 = \DB::insert("insert into evento (cd_dispositivo_alocado, cd_tipo_evento, data_hora, created_at, updated_at) values($id_dispositivo_alocado, enum_busca_id('TIPO_EVENTO', 2), '$data_hora', NOW(), NOW())"); //insert na tabela evento

            }
    
        }catch(\Illuminate\Database\QueryException $ex){
            //ver com a equipe oq fazer em caso de erro
            $mensagem = json_encode($obj);
            $erro  =$ex->getMessage();
            $erro = str_replace(array('\''), '', $erro); 
            $consultax = \DB::insert("insert into logs_integracao (erro_integracao, mensagem, data_hora_mensagem, data_hora_integracao, tipo_mensagem, numero_serie, created_at, updated_at) values ('$erro', '$mensagem', '$data_hora', NOW(), '$tipo_mensagem', '$numero_serie', NOW(), NOW())"); //em caso de erro cria log
            //echo $ex->getMessage();
        }
    }
}
