<?php

namespace App\Console\Commands\Consumers;

use Carbon\Carbon;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class ProcessoConsumerClass{

    public function getMessage($obj) 
    {
        $cd_produto = $obj['dados']['id_produto'];
        $numero_serie = $obj['dados']['numero_serie'];
        $cd_sinal = $obj['dados']['rssi'];
        $data_hora_ultima_comunicacao = $obj['dados']['data_hora_recebimento'];
        $cd_usuario = $obj['dados']['id_usuario'];
        $data_hora_liberacao = $obj['dados']['data_hora_inicio'];
        $data_hora_inicio = $obj['dados']['data_inicio_processo'];
        $data_hora_fim = $obj['dados']['data_fim_processo'];
        $segundos_pausas = $obj['dados']['pausas_tempo'];
        $pausas = $obj['dados']['pausas_numero'];
        $violado = $obj['dados']['violado'];
        if($violado==0) $violado = 'FALSE';
        elseif($violado==1) $violado = 'TRUE';
        else $violado = 'NULL';
        $litro_agua = $obj['dados']['consumo_agua'];
        $vazao_agua_media = $obj['dados']['vazao_media'];
        $energia_kw_h = $obj['dados']['consumo_energia'];
        $observacao = $obj['dados']['observacao'];
        $tipo_mensagem = $obj['evento'];
        try{
            $consulta = \DB::select("select id_dispositivo_alocado from dispositivo_alocado join dispositivo on id_dispositivo = cd_dispositivo
                        where numero_serie = '$numero_serie' and dispositivo_alocado.deleted_at IS NULL");
            $cd_dispositivo_alocado = $consulta[0]->id_dispositivo_alocado;
            $consulta1 = \DB::insert("insert into processo (cd_produto, cd_dispositivo_alocado, cd_usuario, data_hora_liberacao, data_hora_inicio, data_hora_fim, segundos_pausas, pausas, violado, litro_agua, vazao_agua_media, energia_kw_h, observacao, created_at, updated_at) VALUES ($cd_produto, $cd_dispositivo_alocado, $cd_usuario, '$data_hora_liberacao', '$data_hora_inicio', '$data_hora_fim', $segundos_pausas, $pausas, $violado, $litro_agua, $vazao_agua_media, $energia_kw_h, '$observacao', NOW(), NOW())");

            $consulta2 = \DB::update("update dispositivo_alocado set cd_estado = enum_busca_id('ESTADO_DISPOSITIVO', 2), cd_sinal = enum_busca_id('SINAL', $cd_sinal), data_hora_ultima_comunicacao = '$data_hora_ultima_comunicacao, updated_at = NOW() where cd_dispositivo = $cd_dispositivo_alocado");

            $consulta3 = \DB::insert("insert into evento (cd_dispositivo_alocado, cd_tipo_evento, data_hora, created_at, updated_at) values($cd_dispositivo_alocado, enum_busca_id('TIPO_EVENTO', $cd_tipo_evento), '$data_hora_inicio', NOW(), NOW())");
    
        }catch(\Illuminate\Database\QueryException $ex){
            //ver com a equipe oq fazer em caso de erro
            $mensagem = json_encode($obj);
            $erro  =$ex->getMessage();
            $erro = str_replace(array('\''), '', $erro); 
            $consultax = \DB::insert("insert into logs_integracao (erro_integracao, mensagem, data_hora_mensagem, data_hora_integracao, tipo_mensagem, numero_serie, created_at, updated_at) values ('$erro', '$mensagem', '$data_hora_ultima_comunicacao', NOW(), '$tipo_mensagem', '$numero_serie', NOW(), NOW())"); //em caso de erro cria log
            //echo $ex->getMessage();
        }
    }
}
