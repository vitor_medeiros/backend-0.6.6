<?php

namespace App\Console\Commands\Consumers;

use Carbon\Carbon;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class HubSincronizadoConsumerClass{

    public function getMessage($obj) 
    {
        //aqui ficará o tratamento da mensagem
        $numero_serie = $obj['dados']['numero_serie'];
        $cd_sinal = $obj['dados']['cd_sinal'];
        $cd_estado = $obj['dados']['cd_estado'];
        $data_hora_ultima_comunicacao = $obj['dados']['data_hora_sincronizacao'];
        $cd_tipo_evento =  $obj['dados']['codigo_evento'];
        $tipo_mensagem = $obj['evento'];
        $fila = 'hub_' . $numero_serie;
        try{
            
            $connection = new AMQPStreamConnection('amqp.sis.eletromais.tec.br', 5672, 'rabbitmq', 'rabbitmq');
            $channel = $connection->channel();
            $channel->basic_consume($fila, '', false, true, false, false);
            $channel->close();
            $connection->close();
            $consulta = \DB::select("select id_dispositivo_alocado from dispositivo_alocado join dispositivo on id_dispositivo = cd_dispositivo
                where numero_serie = '$numero_serie' and dispositivo_alocado.deleted_at IS NULL");
            $id_dispositivo_alocado = $consulta[0]->id_dispositivo_alocado;
            $consulta1 = \DB::update("update dispositivo_alocado set cd_estado = enum_busca_id('ESTADO_DISPOSITIVO', $cd_estado), cd_sinal = enum_busca_id('SINAL', $cd_sinal), data_hora_ultima_comunicacao = '$data_hora_ultima_comunicacao', updated_at = NOW() where id_dispositivo_alocado = $id_dispositivo_alocado and data_hora_fim_alocacao IS NULL");

            $consulta2 = \DB::insert("insert into evento (cd_dispositivo_alocado, cd_tipo_evento, data_hora, created_at, updated_at) values($id_dispositivo_alocado, enum_busca_id('TIPO_EVENTO', $cd_tipo_evento), '$data_hora_ultima_comunicacao', NOW(), NOW())");

        }catch(\Illuminate\Database\QueryException $ex){
            //ver com a equipe oq fazer em caso de erro
            $mensagem = json_encode($obj);
            $erro  =$ex->getMessage();
            $erro = str_replace(array('\''), '', $erro); 

            $consultax = \DB::insert("insert into logs_integracao (erro_integracao, mensagem, data_hora_mensagem, data_hora_integracao, tipo_mensagem, numero_serie, created_at, updated_at) values ('$erro', '$mensagem', '$data_hora_ultima_comunicacao', NOW(), '$tipo_mensagem', '$numero_serie', NOW(), NOW())"); //em caso de erro cria log
            //echo $ex->getMessage();
        }
    }
}
