<?php

namespace App\Console\Commands\Consumers;

use Carbon\Carbon;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class EstadoConsumerClass{

    public function getMessage($obj) 
    {
        //aqui ficará o tratamento da mensagem
        $numero_serie = $obj['dados']['numero_serie'];
        $cd_sinal = $obj['dados']['rssi'];
        $cd_estado = $obj['dados']['codigo'];
        $data_hora_ultima_comunicacao = $obj['dados']['data_hora_recebimento'];
        $tipo_mensagem = $obj['evento'];
        try{
            $consulta = \DB::update("update dispositivo_alocado set cd_estado = enum_busca_id('ESTADO_DISPOSITIVO', $cd_estado), cd_sinal = enum_busca_id('SINAL', $cd_sinal), data_hora_ultima_comunicacao = '$data_hora_ultima_comunicacao', updated_at = NOW() where cd_dispositivo in (select id_dispositivo from dispositivo where numero_serie = '$numero_serie') and data_hora_fim_alocacao IS NULL");
        }catch(\Illuminate\Database\QueryException $ex){
            //ver com a equipe oq fazer em caso de erro
            $mensagem = json_encode($obj);
            $erro  =$ex->getMessage();
            $erro = str_replace(array('\''), '', $erro); 

            $consultax = \DB::insert("insert into logs_integracao (erro_integracao, mensagem, data_hora_mensagem, data_hora_integracao, tipo_mensagem, numero_serie, created_at, updated_at) values ('$erro', '$mensagem', '$data_hora_ultima_comunicacao', NOW(), '$tipo_mensagem', '$numero_serie', NOW(), NOW())"); //em caso de erro cria log
        }
    }
}
