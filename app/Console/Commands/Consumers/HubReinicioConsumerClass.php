<?php

namespace App\Console\Commands\Consumers;

use Carbon\Carbon;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class HubReinicioConsumerClass{

    public function getMessage($obj) 
    {
        //aqui ficará o tratamento da mensagem
        $numero_serie = $obj['dados']['numero_serie'];
        $data_hora = $obj['dados']['data_hora_reinicio'];
        $tipo_mensagem = $obj['evento'];
        $fila = 'hub_' . $numero_serie;
        try{
            
            $connection = new AMQPStreamConnection('amqp.sis.eletromais.tec.br', 5672, 'rabbitmq', 'rabbitmq');
            $channel = $connection->channel();
            $channel->basic_consume($fila, '', false, true, false, false);
            $channel->close();
            $connection->close();
            
            $message = '{"cd_estacao": $numero_serie, "estacao": true, "usuarios": true, "produtos": true, "dispositivos": true}'; 
            $queue = 'hub_' . $numero_serie;
             
                // echo 'Log error '.$message . PHP_EOL;
            $host = env('AMQP_HOST', 'amqp.sis.eletromais.tec.br');
            $port =  env('AMQP_PORT', 5672);
            $username = env('AMQP_USERNAME', 'rabbitmq');
            $password = env('AMQP_PASSWORD', 'rabbitmq');   
            $connection = new AMQPStreamConnection($host, $port , $username, $password);
            $channel = $connection->channel();
            // Message durability
            $channel->queue_declare($queue, false, true, false, false);
            $msg = new AMQPMessage($message,
                array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
            );
            $channel->basic_publish($msg, '', $queue);
            $channel->close();
            $connection->close();
         
            $consulta = \DB::select("select id_dispositivo_alocado from dispositivo_alocado join dispositivo on id_dispositivo = cd_dispositivo
                where numero_serie = '$numero_serie' and dispositivo_alocado.deleted_at IS NULL");
            $id_dispositivo_alocado = $consulta[0]->id_dispositivo_alocado;

            $consulta1 = \DB::insert("insert into evento (cd_dispositivo_alocado, cd_tipo_evento, data_hora, created_at, updated_at) values($id_dispositivo_alocado, enum_busca_id('TIPO_EVENTO', 2), '$data_hora', NOW(), NOW())");

        }catch(\Illuminate\Database\QueryException $ex){
            //ver com a equipe oq fazer em caso de erro
            $mensagem = json_encode($obj);
            $erro  =$ex->getMessage();
            $erro = str_replace(array('\''), '', $erro); 

            $consultax = \DB::insert("insert into logs_integracao (erro_integracao, mensagem, data_hora_mensagem, data_hora_integracao, tipo_mensagem, numero_serie, created_at, updated_at) values ('$erro', '$mensagem', '$data_hora', NOW(), '$tipo_mensagem', '$numero_serie', NOW(), NOW())"); //em caso de erro cria log
            //echo $ex->getMessage();
        }
    }
}
