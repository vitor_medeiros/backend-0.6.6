<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class GeneratorEstadosMsg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:estado_msg';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $message1 = '{"evento":"estado","dados":{"rssi":3,"numero_serie":"246F28D7543C","tipo":1,"codigo":44,"modo":"C","data_hora_recebimento":"2020-11-18 15:47:03"},"estacao":{"codigo_estacao":10}}';
        $message2 = '{"evento":"processo","dados":{"rssi":1, "numero_serie":"246F28D7543C", "tipo":3, "codigo":1, "modo":"C", "data_hora_recebimento":"09/11/2020 16:29:04", "pausas_numero":"00", "pausas_tempo":"0000", "epoch_inicio":"1604950112", "tempo_liberado":"0004", "tempo_processo":"0020", "consumo_energia":"000", "consumo_agua":"000", "vazao_media":"0000", "violado":"0", "data_hora_inicio":"09/11/2020 16:28:32", "data_inicio_processo":"09/11/2020 16:28:16", "data_fim_processo":"09/11/2020 16:28:36", "id_produto":18, "descricao_produto":"Banho 1,5 m P Cort", "observacao":"cupomfiscal", "id_dispositivo":10, "id_usuario":47, "nome_usuario":"andersonrosa.tuto@yahoo.com", "descricao_evento":"Fim de processo"},"estacao":{"codigo_estacao":10}}';

        $message3 = '{"evento":"evento","dados":{"rssi":3, "numero_serie":"246F28D7543C", "tipo":2, "codigo":4, "modo":"C", "data_hora_recebimento":"05/11/2020 16:43:26", "descricao_evento":"fim_violado"}, "estacao":{"codigo_estacao":10}}';

        $message4 = '{"evento":"tipo_dispositivo","dados":{"rssi":3,"numero_serie":"NOVODISP2","tipo":4,"codigo":2,"modo":"C","data_hora_recebimento":"2020-12-11 08:15:57","versao":"Versão de desenvolvimento","descricao_evento":"Reinicio"},"estacao":{"codigo_estacao":10}}';

        $message5 = '{"evento":"hub_sincronizado", "dados":{"numero_serie":"246F28D7543C", "cd_estado":"1", "cd_sinal":"1", "data_hora_sincronizacao":"2020-12-30 16:43:26", "codigo_evento":101}}';

        $message6 = '{"evento":"hub_reinicio", "dados":{"data_hora_reinicio":"2020-12-30 16:43:26", "numero_serie":"246F28D7543C", "codigo_evento":2}}';

        $queue = 'api_estado';
        $quantity = 1000000;
        // echo 'Log error '.$message . PHP_EOL;

        $host = env('AMQP_HOST', 'amqp.sis.eletromais.tec.br');
        $port =  env('AMQP_PORT', 5672);
        $username = env('AMQP_USERNAME', 'rabbitmq');
        $password = env('AMQP_PASSWORD', 'rabbitmq');
       
        $connection = new AMQPStreamConnection($host, $port , $username, $password);
        $channel = $connection->channel();

        // Message durability
        $channel->queue_declare($queue, false, true, false, false);

        $msg1 = new AMQPMessage($message1,
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $msg2 = new AMQPMessage($message2,
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $msg3 = new AMQPMessage($message3,
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $msg4 = new AMQPMessage($message4,
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $msg5 = new AMQPMessage($message5,
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $msg6 = new AMQPMessage($message6,
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );

        for($i = 0; $i < $quantity; $i++) {
            $channel->basic_publish($msg1, '', $queue);
            //$channel->basic_publish($msg2, '', $queue);
            //$channel->basic_publish($msg3, '', $queue);
            //$channel->basic_publish($msg4, '', $queue);
            //$channel->basic_publish($msg5, '', $queue);
            //$channel->basic_publish($msg6, '', $queue);
            echo Carbon::now(). ' MSG: '.$i. ' Memory:' . memory_get_peak_usage(true), "\n";
        }
        
        $channel->close();
        $connection->close();
                
        return 0;

        
    }
}
