<?php

namespace App\Console\Commands;
use Carbon\Carbon;
use \Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\Console\Commands\Consumers\EstadoConsumerClass;

class EstadoConsumer extends Command{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'estado:consumer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consume as mensagens de estado';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    //handle();
    public function handle()
    {
        function conecta(){
            $connection = new AMQPStreamConnection('amqp.sis.eletromais.tec.br', 5672, 'rabbitmq', 'rabbitmq');
            $channel = $connection->channel();
            $channel->basic_consume('api_estado', '', false, true, false, false, function($message){
                $obj = json_decode($message->body, true);
                $tipo_mensagem = $obj['evento'];  

                if ($tipo_mensagem=='estado') {
                    $evento = new EstadoConsumerClass(); //cria um objeto de acordo com o tipo do evento
                    $evento->getMessage($obj); //chama a função para tratar a mensagem passando como parâmetro o objeto com os dados
                }


            });
            $contador = 0;
            while ($channel->is_consuming()) {
                $channel->wait();
               
                if ($contador==1000) {
                    $channel->close();
                    $connection->close();
                    unset($channel);
                    unset($connection);
                    $contador = 0;
                    conecta();
                }else $contador = $contador + 1;
                
            }
        }
        conecta();
        //$channel->close();
        //$connection->close();
        return 0;
    }
}
