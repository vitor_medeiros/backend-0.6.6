<?php
namespace App\Console\Commands;
require  'vendor/autoload.php';
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
class HubVerificar extends Command{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hub:verificar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    //handle();
    public function handle(){
        //fazer pausa de 1 minuto
        sleep(60);
        try{
            $consulta = \DB::select("select id_dispositivo_alocado, data_hora_ultima_comunicacao from dispositivo_alocado join dispositivo on id_dispositivo = cd_dispositivo
                    join estacao on id_estacao = cd_estacao 
                    where cd_tipo_dispositivo = 1 and dispositivo_alocado.deleted_at IS NULL and cd_situacao = enum_busca_id('SITUACAO_ESTACAO', 3) and cd_estado <> enum_busca_id('ESTADO_DISPOSITIVO', 8)");
            foreach($consulta as $resultado){
                $cd_dispositivo_alocado = $resultado->id_dispositivo_alocado;
                $data_hora_ultima_comunicacao = strtotime($resultado->data_hora_ultima_comunicacao);    
                $dateatual = time();
                $intervalo = abs($dateatual - $data_hora_ultima_comunicacao)/60;
                if ($intervalo > 5) {
                    $consulta2 = \DB::update("update dispositivo_alocado set cd_estado =  enum_busca_id('ESTADO_DISPOSITIVO', 8), updated_at = NOW() where id_dispositivo_alocado = $cd_dispositivo_alocado");
                }
            }
        }catch(\Illuminate\Database\QueryException $ex){
            echo $ex->getMessage();
        }
        return 0;
    }
}
