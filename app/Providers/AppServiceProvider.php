<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Solidos\Core\Utils\SolidosPaginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        $this->app->bind(LengthAwarePaginator::class, SolidosPaginator::class);//para usar um paginador customizado
    }
}
