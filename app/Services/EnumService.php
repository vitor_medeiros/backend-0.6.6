<?php


namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\Enum;

class EnumService extends AbstractService
{
    // Added in cache because Cache::get grow memory for each interations
    // private $enumsInCache = array();

    public function getModelClass(): AbstractModel
    {
        return app(Enum::class);
    }

    private static  function getEnum($entity, $code) {
        // $key = $entity .'_'. $code;
        // if (array_key_exists($key,$this->enumsInCache)) { 
        //     //TODO: Add expiry field to refresh by time
        //     return $this->enumsInCache[$key];
        // }

        // $result = Enum::query()->where( [
        //     'entidade' => $entity,
        //     'codigo' => $code
        // ])->first();

        // $this->enumsInCache[$key] = $result;
        // return $result;

        return Enum::query()->where( [
            'entidade' => $entity,
            'codigo' => $code
        ])->first();
    }

    /**
     * Gets estado disponivel from DB the first time and keep in cache 
     * 
     * @param int $code The enum code
     * 
     * @return Enum entity
     */
    static function getEstadoDispositivo($code) {
        return self::getEnum(Enum::ESTADO_DISPOSITIVO, $code);
    }

    /**
     * Gets sinal from DB the first time and keep in cache 
     * 
     * @param int $code The enum code
     * 
     * @return Enum entity
     */
    static function getSinal($code) {
        return self::getEnum(Enum::SINAL, $code);
    }

    /**
     * Gets situacao estacao from DB the first time and keep in cache 
     * 
     * @param int $code The enum code
     * 
     * @return Enum entity
     */
    static function getSituacaoEstacao($code) {
        return self::getEnum(Enum::SITUACAO_ESTACAO, $code);
    }

    /**
     * Gets tipo contrato from DB the first time and keep in cache 
     * 
     * @param int $code The enum code
     * 
     * @return Enum entity
     */
    static function getTipoContrato($code) {
        return self::getEnum(Enum::TIPO_CONTRATO, $code);
    }

    /**
     * Gets tipo evento from DB the first time and keep in cache 
     * 
     * @param int $code The enum code
     * 
     * @return Enum entity
     */
    static function getTipoEvento($code) {
        return self::getEnum(Enum::TIPO_EVENTO, $code);
    }

}
