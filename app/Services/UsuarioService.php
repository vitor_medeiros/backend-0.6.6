<?php


namespace App\Services;


use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\Usuario;
use App\Models\UsuarioEstacao;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(Usuario::class);
    }

    public function save($data): Model
    {
        try {
            DB::beginTransaction();
            $data['password'] = Hash::make($data['password']);
            $usuario = parent::save($data);
            if (isset($data['usuarioEstacoes'])) {
                foreach ($data['usuarioEstacoes'] as $usuarioEstacao) {
                    $usuarioEstacao['cd_usuario'] = $usuario->id_usuario;
                    $usuarioEstacao['vende'] = $usuarioEstacao['vende'] ?? $usuario->tipo_perfil === 'V';
                    UsuarioEstacao::create($usuarioEstacao);
                }
                $usuario->refresh();
                $usuario->usuarioEstacoes;
            }
            DB::commit();
            return $usuario;
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
