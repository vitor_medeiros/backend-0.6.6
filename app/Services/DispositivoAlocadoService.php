<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\DispositivoAlocado;
use App\Models\EstadoDispositivoAlocado;

class DispositivoAlocadoService extends AbstractService
{
    public function getModelClass(): AbstractModel
    {
        return app(DispositivoAlocado::class);
    }

    public static function getDipositivoAlocado(string $cdDispositivo) {
        return DispositivoAlocado::query()->where('cd_dispositivo', $cdDispositivo)->first();
    }

    /**
     * Atualiza o dispositivo alocado pelo estado
     * 
     * @param EstadoDispositivoAlocado $estado
     * 
     * @throws Exception com a mensage de validação de consistencia
     */
    public static function updateState(EstadoDispositivoAlocado $estado)  {
        $dispositivo = DispositivoService::getDispositivo($estado->numSerie);
        if (null == $dispositivo) {
            throw new \Exception('Dispositivo não encontrado. Número de série: ['.$estado->numSerie.']');
        }

        // Carregar Dispositivo Alocado aonde o campo `delete_at` é nulo
        $dispositivoAlocado = DispositivoAlocadoService::getDipositivoAlocado($dispositivo->id_dispositivo);
        if (null == $dispositivoAlocado) {
            throw new \Exception('Dispositivo não alocado. Código dispositivo: ['.$dispositivo->id_dispositivo.'], Número de serie: '.$estado->numSerie.'');
        } 

        // Código estado
        $enumEstado = EnumService::getEstadoDispositivo($estado->codEstado);
        $dispositivoAlocado->cd_estado = $enumEstado->id_enum;

        // Código sinal
        $enumSinal = EnumService::getEstadoDispositivo($estado->codSinal); 
        $dispositivoAlocado->cd_sinal = $enumSinal->id_enum;
    
        $dispositivoAlocado->data_hora_ultima_comunicacao = $estado->dataHoraUltimaComunicacao;

        // Persistir
        self::saveModel($dispositivoAlocado);
    }
}
