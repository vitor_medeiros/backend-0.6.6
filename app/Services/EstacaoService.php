<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\Estacao;

class EstacaoService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(Estacao::class);
    }
}
