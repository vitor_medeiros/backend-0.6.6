<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\EstadoDispositivo;

class EstadoDispositivoService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(EstadoDispositivo::class);
    }
}
