<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\UsuarioEstacao;

class UsuarioEstacaoService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(UsuarioEstacao::class);
    }
}
