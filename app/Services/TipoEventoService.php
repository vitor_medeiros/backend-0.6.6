<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\TipoEvento;

class TipoEventoService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(TipoEvento::class);
    }
}
