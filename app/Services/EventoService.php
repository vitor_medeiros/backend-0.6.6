<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\Evento;

class EventoService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(Evento::class);
    }
}
