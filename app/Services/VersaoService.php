<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\Versao;

class VersaoService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(Versao::class);
    }
}
