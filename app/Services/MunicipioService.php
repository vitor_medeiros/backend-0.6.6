<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\Municipio;

class MunicipioService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(Municipio::class);
    }
}
