<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\Produto;

class ProdutoService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(Produto::class);
    }
}
