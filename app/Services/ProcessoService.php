<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Core\Interfaces\TagQueueMessage;
use App\Models\Processo;

use Carbon\Carbon;

class ProcessoService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(Processo::class);
    }

    public static function buildEntity(TagQueueMessage $message) {
        $processo = new Processo();
        $processo->cd_produto = $message->getParam('id_produto');
        $processo->cd_dispositivo_alocado = $message->getParam('id_dispositivo');
        $processo->cd_usuario = $message->getParam('id_usuario');
        $processo->data_hora_liberacao = $message->getDateTimeParam('data_hora_inicio');
        $processo->data_hora_inicio = $message->getDateTimeParam('data_inicio_processo');
        $processo->data_hora_fim = $message->getDateTimeParam('data_fim_processo');
        $processo->segundos_pausas = $message->getParam('pausas_tempo');
        $processo->pausas = $message->getParam('pausas_numero');
        $processo->violado = $message->getParam('violado');
        $processo->litro_agua = $message->getParam('consumo_agua');
        $processo->vazao_agua_media = $message->getParam('vazão_media');
        $processo->energia_kw_h = $message->getParam('consumo_energia');
        $processo->observacao = $message->getParam('observacao');
        $processo->created_at = Carbon::now();
        return $processo;
    }
}
