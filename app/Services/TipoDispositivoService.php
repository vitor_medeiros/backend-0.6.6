<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\TipoDispositivo;

class TipoDispositivoService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(TipoDispositivo::class);
    }
}
