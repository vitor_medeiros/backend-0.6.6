<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\VersaoDisponivel;

class VersaoDisponivelService extends AbstractService
{

    public function getModelClass(): AbstractModel
    {
        return app(VersaoDisponivel::class);
    }
}
