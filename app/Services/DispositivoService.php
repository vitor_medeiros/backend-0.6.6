<?php

namespace App\Services;

use App\Core\AbstractModel;
use App\Core\AbstractService;
use App\Models\Dispositivo;

use Illuminate\Support\Facades\Cache;

class DispositivoService extends AbstractService
{
    const DEVICE_ID_BY_SERIAL_NUMBER_CACHE_KEY = 'id_dispositivo_by_numero_serie_key';
    const EXPIRY_CACHE_SECONDS = 60; //

    // Added in cache because Cache::get grow memory for each interations
    private $deviceInCache = array();

    public function getModelClass(): AbstractModel
    {
        return app(Dispositivo::class);
    }

    public static function buildDeviceCacheKey(string $numSerie) {
         return self::DEVICE_ID_BY_SERIAL_NUMBER_CACHE_KEY.'.'.$numSerie;
    }

    public static function getDispositivo(string $numSerie) {
        return Dispositivo::query()->where('numero_serie', $numSerie)->first();
    }
    
}
